package com.prupe.mcpatcher.ctm;

import com.prupe.mcpatcher.*;
import com.prupe.mcpatcher.mal.block.BlockAPI;
import com.prupe.mcpatcher.mal.block.RenderPassAPI;
import com.prupe.mcpatcher.mal.resource.ResourceList;
import net.minecraft.src.*;

import java.util.*;

public class CTMUtils {
    private static final MCLogger logger = MCLogger.getLogger(MCPatcherUtils.CONNECTED_TEXTURES, "CTM");

    private static final boolean enableStandard = Config.getBoolean(MCPatcherUtils.CONNECTED_TEXTURES, "standard", true);
    private static final boolean enableNonStandard = Config.getBoolean(MCPatcherUtils.CONNECTED_TEXTURES, "nonStandard", true);

    private static final List<ITileOverride> allOverrides = new ArrayList<ITileOverride>();
    private static final Map<Block, List<ITileOverride>> blockOverrides = new IdentityHashMap<Block, List<ITileOverride>>();
    private static final Map<String, List<ITileOverride>> tileOverrides = new HashMap<String, List<ITileOverride>>();
    private static TileLoader tileLoader;

    private static ITileOverride lastOverride;
    private static Icon blankIcon;

    private static final TileOverrideIterator.IJK ijkIterator = new TileOverrideIterator.IJK(blockOverrides, tileOverrides);
    private static final TileOverrideIterator.Metadata metadataIterator = new TileOverrideIterator.Metadata(blockOverrides, tileOverrides);

    private static boolean haveBlockFace;
    private static final BlockOrientation blockOrientation = new BlockOrientation();

    static {
        try {
            Class.forName(MCPatcherUtils.RENDER_PASS_CLASS).getMethod("finish").invoke(null);
        } catch (Throwable e) {
        }

        TexturePackChangeHandler.register(new TexturePackChangeHandler(MCPatcherUtils.CONNECTED_TEXTURES, 3) {
            @Override
            public void initialize() {
            }

            @Override
            public void beforeChange() {
                RenderPassAPI.instance.clear();
                try {
                    GlassPaneRenderer.clear();
                } catch (Throwable e) {
                    // nothing
                }
                blockOrientation.clear();
                ijkIterator.clear();
                metadataIterator.clear();
                allOverrides.clear();
                blockOverrides.clear();
                tileOverrides.clear();
                lastOverride = null;
                blankIcon = null;
                tileLoader = new TileLoader("textures/blocks", logger);
                BlockOrientation.reset();
                RenderPassAPI.instance.refreshBlendingOptions();

                if (enableStandard || enableNonStandard) {
                    for (ResourceLocation resource : ResourceList.getInstance().listResources(TexturePackAPI.MCPATCHER_SUBDIR + "ctm", ".properties", true)) {
                        registerOverride(TileOverride.create(resource, tileLoader));
                    }
                }
                for (ResourceLocation resource : BlendMethod.getAllBlankResources()) {
                    tileLoader.preloadTile(resource, false);
                }
            }

            @Override
            public void afterChange() {
                for (ITileOverride override : allOverrides) {
                    override.registerIcons();
                }
                for (Map.Entry<Block, List<ITileOverride>> entry : blockOverrides.entrySet()) {
                    for (ITileOverride override : entry.getValue()) {
                        if (override.getRenderPass() >= 0) {
                            RenderPassAPI.instance.setRenderPassForBlock(entry.getKey(), override.getRenderPass());
                        }
                    }
                }
                for (List<ITileOverride> overrides : blockOverrides.values()) {
                    Collections.sort(overrides);
                }
                for (List<ITileOverride> overrides : tileOverrides.values()) {
                    Collections.sort(overrides);
                }
                setBlankResource();
            }
        });
    }

    private static void clearBlockFace() {
        haveBlockFace = false;
    }

    public static Icon getBlockIcon(Icon icon, RenderBlocks renderBlocks, Block block, IBlockAccess blockAccess, int i, int j, int k, int face) {
        lastOverride = null;
        if (blockAccess != null && checkFace(face)) {
            if (!haveBlockFace) {
                blockOrientation.setBlock(block, blockAccess, i, j, k);
                blockOrientation.setFace(face);
            }
            lastOverride = ijkIterator.go(blockOrientation, icon);
            if (lastOverride != null) {
                icon = ijkIterator.getIcon();
            }
        }
        clearBlockFace();
        return lastOverride == null && skipDefaultRendering(block) ? blankIcon : icon;
    }

    public static Icon getBlockIcon(Icon icon, RenderBlocks renderBlocks, Block block, int face, int metadata) {
        lastOverride = null;
        if (checkFace(face) && checkRenderType(block)) {
            blockOrientation.setup(block, metadata, face);
            lastOverride = metadataIterator.go(blockOrientation, icon);
            if (lastOverride != null) {
                icon = metadataIterator.getIcon();
            }
        }
        return icon;
    }

    public static Icon getBlockIcon(Icon icon, RenderBlocks renderBlocks, Block block, int face) {
        return getBlockIcon(icon, renderBlocks, block, face, 0);
    }

    public static void reset() {
    }

    private static boolean checkFace(int face) {
        return face < 0 ? enableNonStandard : enableStandard;
    }

    private static boolean checkRenderType(Block block) {
        switch (block.getRenderType()) {
            case 11: // fence
            case 21: // fence gate
                return false;

            default:
                return true;
        }
    }

    private static boolean skipDefaultRendering(Block block) {
        return RenderPassAPI.instance.skipDefaultRendering(block);
    }

    private static void registerOverride(ITileOverride override) {
        if (override != null && !override.isDisabled()) {
            boolean registered = false;
            if (override.getMatchingBlocks() != null) {
                for (Block block : override.getMatchingBlocks()) {
                    if (block == null) {
                        continue;
                    }
                    List<ITileOverride> list = blockOverrides.get(block);
                    if (list == null) {
                        list = new ArrayList<ITileOverride>();
                        blockOverrides.put(block, list);
                    }
                    list.add(override);
                    logger.fine("using %s for block %s", override, BlockAPI.getBlockName(block));
                    registered = true;
                }
            }
            if (override.getMatchingTiles() != null) {
                for (String name : override.getMatchingTiles()) {
                    List<ITileOverride> list = tileOverrides.get(name);
                    if (list == null) {
                        list = new ArrayList<ITileOverride>();
                        tileOverrides.put(name, list);
                    }
                    list.add(override);
                    logger.fine("using %s for tile %s", override, name);
                    registered = true;
                }
            }
            if (registered) {
                allOverrides.add(override);
            }
        }
    }

    static void setBlankResource() {
        blankIcon = tileLoader.getIcon(RenderPassAPI.instance.getBlankResource());
    }

    public static class Ext18 {
        public static void setBlock(IBlockAccess blockAccess, Block block, Position position) {
            blockOrientation.setBlock(block, blockAccess, position.getI(), position.getJ(), position.getK());
            blockOrientation.setFakeRenderType();
        }

        public static void setFace(Direction paramFace, Direction textureFace, Direction blockFace, BlockModelFace modelFace) {
            haveBlockFace = true;
            int blockFaceNum;
            if (paramFace != null) {
                blockFaceNum = paramFace.ordinal();
            } else if (blockFace != null) {
                blockFaceNum = blockFace.ordinal();
            } else {
                blockFaceNum = -1;
            }
            int rotation = getRotation(modelFace, blockFaceNum);
            blockOrientation.setFace(blockFaceNum, textureFace == null ? -1 : textureFace.ordinal(), rotation);
            if (blockOrientation.logIt()) {
                logger.info("%s:%d @ %d,%d,%d p=%s t=%s b=%s -> %d rotation %d",
                    BlockAPI.getBlockName(blockOrientation.block), blockOrientation.metadata,
                    blockOrientation.i, blockOrientation.j, blockOrientation.k,
                    paramFace, textureFace, blockFace, blockFaceNum, rotation
                );
                logRotation(modelFace, blockFaceNum);
                int[] offset = blockOrientation.getOffset(BlockOrientation.REL_U);
                logger.info("  rel up=%d %d %d", offset[0], offset[1], offset[2]);
                offset = blockOrientation.getOffset(BlockOrientation.REL_D);
                logger.info("  rel down=%d %d %d", offset[0], offset[1], offset[2]);
                offset = blockOrientation.getOffset(BlockOrientation.REL_L);
                logger.info("  rel left=%d %d %d", offset[0], offset[1], offset[2]);
                offset = blockOrientation.getOffset(BlockOrientation.REL_R);
                logger.info("  rel right=%d %d %d", offset[0], offset[1], offset[2]);
            }
        }

        private static int getRotation(BlockModelFace modelFace, int face) {
            return getXYZRotation(modelFace, face) - getUVRotation(modelFace);
        }

        private static int getUVRotation(BlockModelFace modelFace) {
            int[] b = modelFace.getShadedIntBuffer();
            float du = Float.intBitsToFloat(b[4]) - Float.intBitsToFloat(b[18]); // u0 - u2
            float dv = Float.intBitsToFloat(b[5]) - Float.intBitsToFloat(b[19]); // v0 - v2
            return getRotation(du, dv);
        }

        private static int getXYZRotation(BlockModelFace modelFace, int face) {
            int[] b = modelFace.getShadedIntBuffer();
            float dx = Float.intBitsToFloat(b[0]) - Float.intBitsToFloat(b[14]); // x0 - x2
            float dy = Float.intBitsToFloat(b[1]) - Float.intBitsToFloat(b[15]); // y0 - y2
            float dz = Float.intBitsToFloat(b[2]) - Float.intBitsToFloat(b[16]); // z0 - z2
            float du;
            float dv;
            switch (face) {
                case BlockOrientation.BOTTOM_FACE:
                    du = dx;
                    dv = -dz;
                    break;

                case BlockOrientation.TOP_FACE:
                    du = dx;
                    dv = dz;
                    break;

                case BlockOrientation.NORTH_FACE:
                    du = -dx;
                    dv = -dy;
                    break;

                case BlockOrientation.SOUTH_FACE:
                    du = dx;
                    dv = -dy;
                    break;

                case BlockOrientation.WEST_FACE:
                    du = dz;
                    dv = -dy;
                    break;

                case BlockOrientation.EAST_FACE:
                    du = -dz;
                    dv = -dy;
                    break;

                default:
                    return 0;
            }
            return getRotation(du, dv);
        }

        private static int getRotation(float s, float t) {
            if (s <= 0) {
                if (t <= 0) {
                    return 0; // no rotation
                } else {
                    return 2; // rotate 90 ccw
                }
            } else {
                if (t <= 0) {
                    return 6; // rotate 90 cw
                } else {
                    return 4; // rotate 180
                }
            }
        }

        private static void logRotation(BlockModelFace modelFace, int face) {
            int[] b = modelFace.getShadedIntBuffer();
            int x0 = (int) (Float.intBitsToFloat(b[0]) * 16.0f);
            int y0 = (int) (Float.intBitsToFloat(b[1]) * 16.0f);
            int z0 = (int) (Float.intBitsToFloat(b[2]) * 16.0f);
            int u0 = (int) Float.intBitsToFloat(b[4]);
            int v0 = (int) Float.intBitsToFloat(b[5]);

            int x1 = (int) (Float.intBitsToFloat(b[7]) * 16.0f);
            int y1 = (int) (Float.intBitsToFloat(b[8]) * 16.0f);
            int z1 = (int) (Float.intBitsToFloat(b[9]) * 16.0f);
            int u1 = (int) Float.intBitsToFloat(b[11]);
            int v1 = (int) Float.intBitsToFloat(b[12]);

            int x2 = (int) (Float.intBitsToFloat(b[14]) * 16.0f);
            int y2 = (int) (Float.intBitsToFloat(b[15]) * 16.0f);
            int z2 = (int) (Float.intBitsToFloat(b[16]) * 16.0f);
            int u2 = (int) Float.intBitsToFloat(b[18]);
            int v2 = (int) Float.intBitsToFloat(b[19]);

            int x3 = (int) (Float.intBitsToFloat(b[21]) * 16.0f);
            int y3 = (int) (Float.intBitsToFloat(b[22]) * 16.0f);
            int z3 = (int) (Float.intBitsToFloat(b[23]) * 16.0f);
            int u3 = (int) Float.intBitsToFloat(b[25]);
            int v3 = (int) Float.intBitsToFloat(b[26]);

            logger.info("x0,y0,z0=%d %d %d, x1,y1,z1=%d %d %d, x2,y2,z2=%d %d %d, x3,y3,z3=%d %d %d, rotation %d",
                x0, y0, z0, x1, y1, z1, x2, y2, z2, x3, y3, z3, getXYZRotation(modelFace, face)
            );
            logger.info("u0,v0=%d %d, u1,v1=%d %d, u2,v2=%d %d, u3,v3=%d %d, rotation %d",
                u0, v0, u1, v1, u2, v2, u3, v3, getUVRotation(modelFace)
            );
        }
    }
}
